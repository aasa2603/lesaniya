import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-common-header',
  templateUrl: './common-header.component.html',
  styleUrls: ['./common-header.component.scss']
})
export class CommonHeaderComponent implements OnInit {
  loggedInUserId;
  userHasLoggedIn = false;
  constructor(private router: Router) { }

  ngOnInit() {
    this.loggedInUserId = localStorage.getItem('userID');
    if(this.loggedInUserId) {
      this.userHasLoggedIn = true;
    } else {
      this.userHasLoggedIn = false;
    }
  }

  logoutClicked() {
    localStorage.removeItem('userID');
  }

  navigateToSignUp() {
    this.router.navigate(['/signup']);
    this.closeNavbar();
  }

  closeNavbar() {
    var navbar= document.getElementById("navbarResponsive");
    navbar.style.display = "none";
  }
}
