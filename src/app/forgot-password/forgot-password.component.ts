import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  FindEmailForm: FormGroup
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService
    ) { }

  ngOnInit() {
    this.FindEmailForm = this.formBuilder.group({
      email :['', [Validators.required, Validators.email]],
      });
  }


  get formControls() { return this.FindEmailForm.controls; }

  findEmail() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.FindEmailForm.invalid) {
        return;
    }
    this.sendDataToMail(this.FindEmailForm)
  }

  sendDataToMail(form) {
    const sendData = {
      email : form.value.email
    }
  this.apiService.sendMail(sendData).subscribe((res)=> {
    console.log("response",res)
  })
  }
}