import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  registerUser(signupData){
    console.log(signupData, "in service");
    return this.httpClient.post(`${this.baseUrl}/addsignupData`, signupData);
  }

  loginUser(loginData){
    console.log(loginData, "in service");
    return this.httpClient.post(`${this.baseUrl}/login`, loginData);
  }

  sendMail(maildata) {
    console.log(maildata, "in service");
    return this.httpClient.post(`${this.baseUrl}/sendmail`, maildata);
  }

  getAllUser() {
    return this.httpClient.get(`${this.baseUrl}/getsignupData`)
  }

  getUser(id) {
    return this.httpClient.get(`${this.baseUrl}/getUser/${id}`)
  }

  addConfession(data) {
    return this.httpClient.post(`${this.baseUrl}/addConfession/?sentId=${data.senderId}&receiveId=${data.userId}`, data.confession)
  }

  addProfileImage(profileData, data){
    console.log(profileData, "in service");
    const profileEmail = data.email;
    return this.httpClient.post(`${this.baseUrl}/profile-photo-upload/${profileEmail}`, profileData);
  }

  sendFeedback(feedbackData) {
    return this.httpClient.post(`${this.baseUrl}/sendFeedbackMail`, feedbackData)
  }
  searchUser(q) {
    return this.httpClient.get(`${this.baseUrl}/search?q=${q}`);
  }
}
