import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{MatButtonModule, MatFormFieldModule} from '@angular/material'
import {MatAutocompleteModule} from '@angular/material/autocomplete';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  exports :[
    MatAutocompleteModule,
    MatFormFieldModule 

  ]
})
export class MaterialModuleModule { }
