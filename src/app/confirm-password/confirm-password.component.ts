import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-confirm-password',
  templateUrl: './confirm-password.component.html',
  styleUrls: ['./confirm-password.component.scss']
})
export class ConfirmPasswordComponent implements OnInit {
  ResponseResetForm: FormGroup
  IsResetFormValid = true;
  submitted = false;
  passwordNotMatch = false;
  
  constructor(private formBuilder: FormBuilder, ) { }

  ngOnInit() {
    this.checkValidations()
  }
  checkValidations() {
    this.ResponseResetForm = this.formBuilder.group(
      {
        email :['', [Validators.required, Validators.email]],
        newPassword: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
      }
    );
  }

  get formControls() { return this.ResponseResetForm.controls; }

  Validate(passwordFormGroup) {
    const Password = passwordFormGroup.controls.newPassword.value;
    const confirm_password = passwordFormGroup.controls.confirmPassword.value;

    if (confirm_password.length <= 0) {
      return null;
    }

    if (confirm_password !== Password) {
      this.passwordNotMatch = true
      return {
        doesNotMatch: false
      };
    }

    return true;
  }

  ResetPassword(form) {
    this.submitted = true
    const temp_ = this.Validate(this.ResponseResetForm)
    if (temp_ == true) {
      this.passwordNotMatch = false
      if (form.valid) {
        this.IsResetFormValid = true;

      } else { this.IsResetFormValid = false; }
    }
  }
}
