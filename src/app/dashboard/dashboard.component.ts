import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor( 
    // private dialog: MatDialog,
    config: NgbCarouselConfig
  ) {
    config.interval = 2000;  
    config.wrap = true;  
    config.keyboard = false;  
    config.pauseOnHover = false;
   }
  

  ngOnInit() {
  }
  // openLogIn() {
  //   const dialogConfig = new MatDialogConfig();
  //   this.dialog.open(LoginComponent,{
  //     height: '80%',
  //     width: '560px', 
  //   })
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  // }
}
