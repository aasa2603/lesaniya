import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { CommonHeaderComponent } from './common-header/common-header.component';
import { ConfirmPasswordComponent } from './confirm-password/confirm-password.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfessionPostComponent } from './confession-post/confession-post.component';
import { SupportComponent } from './support/support.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'forgotpassword' ,component :  ForgotPasswordComponent },
  { path: 'change-password' ,component :  ConfirmPasswordComponent },
  { path: 'common-header', component:  CommonHeaderComponent },
  { path: 'dashboard', component:  DashboardComponent },
  { path: 'post-confession', component:  ConfessionPostComponent },
  { path: 'contact-us', component:  SupportComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
