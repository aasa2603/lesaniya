import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { ApiService } from '../../app/services/api.service'
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-confession-post',
  templateUrl: './confession-post.component.html',
  styleUrls: ['./confession-post.component.scss']
})
export class ConfessionPostComponent implements OnInit {
  userId = '';
  senderId = '';
  confession = '';
  fname = '';
  lname = '';
  profilePhotoPath = '';
  email = '';
  constructor(
    private router: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private apiService: ApiService
  ) {
    this.userId = this.router.snapshot.queryParamMap.get('userId')
    this.senderId = localStorage.getItem('userID')
    console.log('this.userId', this.userId)
    console.log("senderId", this.senderId)
  }

  ngOnInit() {
    this.getUserDetails(this.userId);
  }

 getUserDetails(id) {
  this.apiService.getUser(id).subscribe(res => {
    if (res) {
      this.fname = res['result'].firstName;
      this.lname = res['result'].lastName;
      this.email = res['result'].email;
      this.profilePhotoPath = res['result'].profilePhotoPath;
      console.log(this.profilePhotoPath);
    }
  })
 }

  addConfession() {
    if (this.confession == "") {
      console.log("please enter confession")
      return;
    } else if(this.confession) {
      const sendData = {
        userId: this.userId,
        senderId: this.senderId,
        confession: { confession: this.confession }
      }
      this.apiService.addConfession(sendData).subscribe(res => {
        console.log("res===>>>>", res)
      })
      this.openSnackBar("confession sent","");
      // window.location.reload();
      this.confession = "";
    }
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      verticalPosition: 'top', 
      horizontalPosition: 'end',
      duration: 2000,
      panelClass: ['red-snackbar'],
    });
  }
}
