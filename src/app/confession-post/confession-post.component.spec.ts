import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfessionPostComponent } from './confession-post.component';

describe('ConfessionPostComponent', () => {
  let component: ConfessionPostComponent;
  let fixture: ComponentFixture<ConfessionPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfessionPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfessionPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
