import { Component } from '@angular/core';
import { Event, Router, NavigationStart, NavigationEnd } from '@angular/router'
@Component({
  selector: 'app-root',
  // template: `<app-common-header></app-common-header>`,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  showSpinner: boolean = true
  title = 'Confess Love Anonymously';
  constructor(private router: Router) {
    this.router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.showSpinner = true
      }
      if (routerEvent instanceof NavigationEnd) {
        this.showSpinner = false
      }
    })

  }
}
