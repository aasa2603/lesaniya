import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MatDialog } from '@angular/material/dialog';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: ApiService,
    private dialog: MatDialog,
    private activateRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userName: ['', [Validators.required, Validators.pattern('^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$'), Validators.maxLength(20)]],
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$')]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
    });
  }
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else {
    // 
    console.log(this.registerForm);
    var data = {
      "firstName": this.registerForm.value.firstName,
      "lastName": this.registerForm.value.lastName,
      "email": this.registerForm.value.email,
      "userName": this.registerForm.value.userName,
      "password": this.registerForm.value.password,
      "confirmPassword": this.registerForm.value.confirmPassword
    }

    this.service.registerUser(data)
      .subscribe((response) => {
      if(response){
        this.openSnackBar("Sucessfully Login","");
      } else {
        this.openSnackBar("Something is wrong","");
      }
        console.log(response);
      });
    }
  }

  openLogIn() {
    console.log("clicked");
    this.router.navigate(['/login']);
  }

  openForget() {
    this.dialog.open(ForgotPasswordComponent,{
      height: '60%',
      width: '40%', 
     })
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      verticalPosition: 'top', 
      horizontalPosition: 'end',
      duration: 2000,
      panelClass: ['red-snackbar'],
    });
  }
}



