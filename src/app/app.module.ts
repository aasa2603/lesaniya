import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { MaterialModuleModule } from './material-module/material-module.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { CommonHeaderComponent } from './common-header/common-header.component';
import { ConfirmPasswordComponent } from './confirm-password/confirm-password.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './profile/profile.component';

import { SocialLoginModule, AuthServiceConfig, LoginOpt } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatDialogModule, MatSnackBarModule } from '@angular/material';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfessionPostComponent } from './confession-post/confession-post.component';
import { LoaderComponent } from './loader/loader.component';
import { SupportComponent } from './support/support.component';
import { CommonFooterComponent } from './common-footer/common-footer.component';

const fbLoginOptions: LoginOpt = {
  scope: 'pages_messaging,pages_messaging_subscriptions,email,pages_show_list,manage_pages',
  return_scopes: true,
  enable_profile_selector: true
};

const googleLoginOptions: LoginOpt = {
  scope: 'profile email'
};

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("594107568722-r5ts486pl16hg9hvg20kkd26pmgjeeqp.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("2775601429154429")
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    ForgotPasswordComponent,
    CommonHeaderComponent,
    ConfirmPasswordComponent,
    ProfileComponent,
    DashboardComponent,
    ConfessionPostComponent,
    LoaderComponent,
    SupportComponent,
    CommonFooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    MaterialModuleModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SocialLoginModule,
    FontAwesomeModule,
    MatDialogModule,
    MatSnackBarModule,
    NgbModule
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: provideConfig
  }],
  bootstrap: [AppComponent],
  entryComponents:[ForgotPasswordComponent]
})

export class AppModule { }
