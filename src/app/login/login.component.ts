
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { faFacebook, faGoogle } from "@fortawesome/free-brands-svg-icons";
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  logInForm: FormGroup;
  submitted = false;
  faFacebook = faFacebook;
  faGoogle = faGoogle;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private apiService: ApiService,
    private router: Router,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.logInForm = this.formBuilder.group({
      userName: ['', Validators.required],
      Password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  get f() { return this.logInForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.logInForm.invalid) {
      return;
    }
    this.loginUser(this.logInForm)

  }

  loginUser(form) {
    const sendData = {
      email: form.value.userName,
      password: form.value.Password
    }
    this.apiService.loginUser(sendData).subscribe((res) => {
      console.log("res",res)
      this.openSnackBar("Sucessfully Login","");
      console.log("response==>>", res['userData'][0]._id)
      localStorage.setItem('userID',res['userData'][0]._id)
      if (res) {
        this.router.navigate(['/dashboard'])
        .then(() => {
          window.location.reload();
        });
      } else {
        this.openSnackBar("Somthing is wrong","");
      }
    })
  }

  signInWithGoogle(): void {
    console.log("google");
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res) => {
      console.log("res===>>>", res)
      if(res) {
        this.openSnackBar("Sucessfully Login","");
      }
    });
    console.log("google1");
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.openSnackBar("Sucessfully Login","");
  }

  signOut(): void {
    this.authService.signOut();
  }

  openSignUp() {
    this.router.navigate(['/signup']);
  }
  
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      verticalPosition: 'top', 
      horizontalPosition: 'end',
      duration: 2000,
      panelClass: ['red-snackbar'],
    });
  }

}
