import { Component, OnInit } from '@angular/core';
import { faFacebook, faGoogle } from "@fortawesome/free-brands-svg-icons"

@Component({
  selector: 'app-common-footer',
  templateUrl: './common-footer.component.html',
  styleUrls: ['./common-footer.component.scss']
})
export class CommonFooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
