import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms'
import { Observable } from 'rxjs';
import { debounce } from 'lodash';
import { map, startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { faSearchengin } from "@fortawesome/free-brands-svg-icons"

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  faSearchengin = faSearchengin;
  showSpinner: boolean = true
  sentMessage = []
  receiveMessage = []
  fname = ''
  lname = ''
  email = ''
  userdata: any
  userControl = new FormControl();
  selectedFile: File
  imagePreview
  showUploadeImage = false
  profilePhotoPath = ""
  originalFilename;
  profileImageResponse;
  imageName = "";
  filteredUsers: "";
  // users = []
  constructor(
    private apiService: ApiService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {
    // this.search = debounce(this.search, 1000)
  }

  search(e) {
    const event = e.target.value;
    console.log(event);
    this.apiService.searchUser(event).subscribe(res => {
      console.log("res>>>>", res);
      if (res['data']) {
        this.filteredUsers = res['data'];
      } else {
        this.filteredUsers = "";
      }
    });
  }

  ngOnInit() {
    this.getAllUser();
  }

  getAllUser() {
    this.apiService.getAllUser().subscribe(res => {
      if (res) {
        this.showSpinner = false
        // this.concatfirstNamelastName(res);
        this.getUser();
      }
    })
  }

  getUser() {
    let id = localStorage.getItem('userID')
    this.apiService.getUser(id).subscribe(res => {
      if (res) {
        this.fname = res['result'].firstName
        this.lname = res['result'].lastName
        this.email = res['result'].email
        this.sentMessage = res['result'].sentMessage
        this.receiveMessage = res['result'].receiveMessage
        this.profilePhotoPath = res['result'].profilePhotoPath
        console.log(this.profilePhotoPath)
      }
    })
  }

  goToUserPage(user: any) {
    console.log("data==>>>>>", user)
    this.router.navigate(['/post-confession'], { queryParams: { userId: user._id } });
  }

  submitImageData(e) {
    var fileName = e.target.files[0];
    const uploadData = new FormData();
    uploadData.append('file', fileName, fileName.name);
    console.log(uploadData);
    const data = {
      email: this.email
    }

    this.apiService.addProfileImage(uploadData, data)
      .subscribe((response) => {
        console.log(response);
        this.profileImageResponse = response;
        this.imageName = this.profileImageResponse.file.filename;
      });
    this.openSnackBar("Sucessfully uploaded", "");
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      verticalPosition: 'top',
      horizontalPosition: 'end',
      duration: 2000,
      panelClass: ['red-snackbar'],
    });
  }

}
