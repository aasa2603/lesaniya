import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {
  supportForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: ApiService,
  ) { }

  ngOnInit() {
    this.supportForm = this.formBuilder.group({
      name: ['', Validators.required],
      subject: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$')]],
      feedback:['', Validators.required],
    });
  }

  get f() { return this.supportForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.supportForm.invalid) {
      return;
    } else {
    var data = {
      "name": this.supportForm.value.name,
      "subject": this.supportForm.value.subject,
      "email": this.supportForm.value.email,
      "feedback": this.supportForm.value.feedback,
    }

    this.service.sendFeedback(data)
      .subscribe((response) => {
        console.log(response);
        if(response) {
          this.supportForm.reset();
        }
      });
      
    }

  }


}
